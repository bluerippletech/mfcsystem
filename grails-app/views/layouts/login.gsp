<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HTML5 Admin Simplenso Template">
    <meta name="author" content="ahoekie">
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">--}%
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">--}%

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
    <r:require modules="application,jquery"/>
    <g:layoutHead/>
    <r:layoutResources/>
</head>
<body id="dashboard">
<!-- Main Content Area | Side Nav | Content -->
<div class="container-fluid">
    <div class="row-fluid">
        <div class="span4">&nbsp;</div>
        <div class="span4">
            <div class="page-header">
                <h1><g:message code="com.ripple.scurity.login.header" default="Login"/></h1>
            </div>
            <div class="box" style="margin-bottom:500px;">
                <h4 class="box-header round-top"><g:message code="com.ripple.scurity.login.header2" default="Login"/></h4>
                <div class="box-container-toggle">
                    <g:layoutBody/>
                </div>
            </div>
            <!--/span-->
            <div class="span4">&nbsp;</div>
        </div>
        <!--/span-->
    </div>
    <!--/row-->

    <footer>
        <p class="pull-right">&copy; Simplenso 2012</p>
    </footer>
</div>
<!--/.fluid-container-->
</html>