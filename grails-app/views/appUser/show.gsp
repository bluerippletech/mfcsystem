
<%@ page import="com.ripple.security.AppUser" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'appUser.label', default: 'AppUser')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-appUser" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                            
                            <g:if test="${appUserInstance?.username}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="username-label" class="control-label"><g:message
                                            code="appUser.username.label"
                                            default="Username"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="username-label"><g:fieldValue
                                            bean="${appUserInstance}" field="username"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${appUserInstance?.password}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="password-label" class="control-label"><g:message
                                            code="appUser.password.label"
                                            default="Password"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="password-label"><g:fieldValue
                                            bean="${appUserInstance}" field="password"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${appUserInstance?.accountExpired}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="accountExpired-label" class="control-label"><g:message
                                            code="appUser.accountExpired.label"
                                            default="Account Expired"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="accountExpired-label"><g:formatBoolean
                                            boolean="${appUserInstance?.accountExpired}"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${appUserInstance?.accountLocked}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="accountLocked-label" class="control-label"><g:message
                                            code="appUser.accountLocked.label"
                                            default="Account Locked"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="accountLocked-label"><g:formatBoolean
                                            boolean="${appUserInstance?.accountLocked}"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${appUserInstance?.enabled}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="enabled-label" class="control-label"><g:message
                                            code="appUser.enabled.label"
                                            default="Enabled"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="enabled-label"><g:formatBoolean
                                            boolean="${appUserInstance?.enabled}"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                            <g:if test="${appUserInstance?.passwordExpired}">
                                <div class="form-horizontal">
                                <div class="control-group">
                                    <h5 id="passwordExpired-label" class="control-label"><g:message
                                            code="appUser.passwordExpired.label"
                                            default="Password Expired"/></h5>
                                    <div class="controls" style="margin-top:4px">
                                    
                                    <span class="property-value" aria-labelledby="passwordExpired-label"><g:formatBoolean
                                            boolean="${appUserInstance?.passwordExpired}"/></span>
                                    
                                    </div>
                                </div>
                                </div>
                            </g:if>
                            
                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="${appUserInstance?.id}"/>
                                %{--<g:link class="edit" action="edit" id="${appUserInstance?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
