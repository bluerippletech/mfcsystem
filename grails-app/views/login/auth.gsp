<html>
<head>
	<meta name='layout' content='login'/>
	<title><g:message code="springSecurity.login.title"/></title>
    <r:require module="all"/>
</head>

<body>
    <div class="box-content">
        <g:if test='${flash.message}'>
            <div class='login_message'>${flash.message}</div>
        </g:if>
        <form action='${postUrl}' method='POST' id='loginForm' class="well form-search" autocomplete='off'>
        <p>
            <label for='username' style="width: 25%; text-align: right;"><g:message code="springSecurity.login.username.label"/>:</label>
            <input type='text' class='text_' name='j_username' id='username'/>
        </p>
        <p>
            <label for='password' style="width: 25%; text-align: right;"><g:message code="springSecurity.login.password.label"/>:</label>
            <input type='password' class='text_' name='j_password' id='password'/>
        </p>
        <p id="remember_me_holder">
            <label for='' style="width: 25%; text-align: right;"></label>
            <input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
            <label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
        </p>
        <p>
            <label for='' style="width: 25%; text-align: right;"></label>
            <input type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>
        </p>
        </form>
    </div>
    <script type='text/javascript'>
        <!--
        (function() {
            document.forms['loginForm'].elements['j_username'].focus();
        })();
        // -->
    </script>
</body>
</html>
