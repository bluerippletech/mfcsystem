<head>
<meta name='layout' content='login' />
<title><g:message code="springSecurity.denied.title" /></title>
<r:require module="all"/>
</head>

<body>
<div class="box-content">
	<div class='errors'><g:message code="springSecurity.denied.message" /></div>
</div>
</body>
