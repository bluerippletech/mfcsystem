<%@ page import="com.ripple.master.Customer" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'customer.label', default: 'Customer')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        ${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12" id="edit-customer" role="main">
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>

                        <div class="row-fluid">
                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="companyName-label" class="control-label"><g:message
                                                code="customer.companyName.label"
                                                default="Company Name"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="companyName-label"><g:fieldValue
                                                    bean="${customerInstance}" field="companyName"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="telephoneNumber-label" class="control-label"><g:message
                                                code="customer.telephoneNumber.label"
                                                default="Telephone Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="telephoneNumber-label"><g:fieldValue
                                                    bean="${customerInstance}" field="telephoneNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="faxNumber-label" class="control-label"><g:message
                                                code="customer.faxNumber.label"
                                                default="Fax Number"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="faxNumber-label"><g:fieldValue
                                                    bean="${customerInstance}" field="faxNumber"/></span>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="span6 column">

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="companyAddress-label" class="control-label"><g:message
                                                code="customer.companyAddress.label"
                                                default="Company Address"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="companyAddress-label"><g:fieldValue
                                                    bean="${customerInstance}" field="companyAddress"/></span>

                                        </div>
                                    </div>
                                </div>


                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="deliveryAddress-label" class="control-label"><g:message
                                                code="customer.deliveryAddress.label"
                                                default="Delivery Address"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value"
                                                  aria-labelledby="deliveryAddress-label"><g:fieldValue
                                                    bean="${customerInstance}" field="deliveryAddress"/></span>

                                        </div>
                                    </div>
                                </div>

                                <div class="form-horizontal">
                                    <div class="control-group">
                                        <h5 id="contactPersons-label" class="control-label"><g:message
                                                code="customer.contactPersons.label"
                                                default="Contact Persons"/></h5>

                                        <div class="controls" style="margin-top:4px">

                                            <span class="property-value" aria-labelledby="contactPersons-label">
                                                <ul>
                                                    <g:each in="${customerInstance?.contactPersons}" var="contacts">
                                                        <li>
                                                            ${contacts.toString()}
                                                        </li>
                                                    </g:each>
                                                </ul>
                                            </span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </fieldset>

                    <div class="form-actions form-horizontal">
                        <g:form>
                            <g:hiddenField name="id" value="${customerInstance?.id}"/>
                            <g:actionSubmit class="btn btn-primary" action="edit"
                                            value="${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                            <g:actionSubmit class="delete btn-danger" action="delete"
                                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
