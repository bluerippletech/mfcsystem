<%@ page import="com.ripple.master.Customer" %>



<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'companyName', 'error')} required">
	<label for="companyName">
		<g:message code="customer.companyName.label" default="Company Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="companyName" cols="40" rows="5" maxlength="255" required="" value="${customerInstance?.companyName}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'companyAddress', 'error')} required">
	<label for="companyAddress">
		<g:message code="customer.companyAddress.label" default="Company Address" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="companyAddress" cols="40" rows="5" maxlength="2000" required="" value="${customerInstance?.companyAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'deliveryAddress', 'error')} ">
	<label for="deliveryAddress">
		<g:message code="customer.deliveryAddress.label" default="Delivery Address" />
		
	</label>
	<g:textArea name="deliveryAddress" cols="40" rows="5" maxlength="2000" value="${customerInstance?.deliveryAddress}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'telephoneNumber', 'error')} required">
	<label for="telephoneNumber">
		<g:message code="customer.telephoneNumber.label" default="Telephone Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="telephoneNumber" maxlength="50" required="" value="${customerInstance?.telephoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'faxNumber', 'error')} required">
	<label for="faxNumber">
		<g:message code="customer.faxNumber.label" default="Fax Number" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="faxNumber" maxlength="50" required="" value="${customerInstance?.faxNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: customerInstance, field: 'contactPersons', 'error')} ">
	<label for="contactPersons">
		<g:message code="customer.contactPersons.label" default="Contact Persons" />
		
	</label>
	
</div>

