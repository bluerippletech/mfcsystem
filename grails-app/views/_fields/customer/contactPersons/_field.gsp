<%@ page defaultCodec="html" %>
<div class="control-group ${invalid ? 'error' : ''}">
    <label class="control-label" for="${property}">${label}</label>

    <div class="controls">
        <input type="text" name="contactPerson" id="contactPerson"/>&nbsp;<a class="btn-small round-all btn-info"
                                                                             href="#"
                                                                             onclick="javascript:submitAjaxAndRefreshListing()">Add</a>
        <g:if test="${invalid}"><span class="help-inline">${errors.join('<br>')}</span></g:if>
        <ul id="contactPersons">
            <g:each in="${value}" var="contactPerson">
                <li><a href="#" onclick="deleteContactPerson(this)">${contactPerson}</a></li>
            </g:each>
        </ul>
    </div>
</div>
<r:script>
function submitAjaxAndRefreshListing(){
    $.ajax({post:'GET',
            url:'${createLink(controller: 'customer', action: 'remoteAddContactPerson')}',
            data:{id:'${bean.id}',contactPerson:$("#contactPerson").val()},
            success:function(responseText){
                $("#contactPerson").val("");
                $("#contactPersons").html("");
                _.each(responseText,function(num){$("#contactPersons").append("<li><a href='#' onclick='deleteContactPerson(this);'>"+num+"</a></li>")});
            },
            error:function(err){
                alert('error encountered');
            }
    });
}

function deleteContactPerson(contactPersonName){
    $.ajax({post:'GET',
            url:'${createLink(controller: 'customer', action: 'remoteDeleteContactPerson')}',
            data:{id:'${bean.id}',contactPerson:contactPersonName.innerHTML},
            success:function(responseText){
                $("#contactPerson").val("");
                $("#contactPersons").html("");
                _.each(responseText,function(num){$("#contactPersons").append("<li><a href='#' onclick='deleteContactPerson(this);'>"+num+"</a></li>")});
            },
            error:function(err){
                alert('error encountered');
            }
    });
}

$("#contactPerson").keypress(function(event) {
    if (event.which == 13){
        submitAjaxAndRefreshListing();
        return false;
    }
});
</r:script>