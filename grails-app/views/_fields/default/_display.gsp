<div class="control-group">
    <h5 id="${property}-label" class="control-label">${label}</h5>
    <div class="controls" style="margin-top:4px">
        <span class="property-value"
              aria-labelledby="${property}-label"><g:fieldValue
                bean="${bean}" field="${property}"/></span>

    </div>
</div>