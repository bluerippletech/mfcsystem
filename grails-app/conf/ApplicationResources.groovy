modules = {

    'bootstrapjs' {
        dependsOn 'bootstrap-js'
        resource url:'bootstrap/css/bootstrap-responsive.min.css'
        resource url:'bootstrap/css/bootstrap.min.css'
        resource url:'bootstrap/img/glyphicons-halflings-white.png'
        resource url:'bootstrap/img/glyphicons-halflings.png'
    }

    application {
//        resource url:'js/application.js'
        resource url: 'css/themes/cerulean/simplenso.cerulean.css'
        resource url: 'css/themes/cerulean/bootstrap.min.css'
        resource url: 'css/themes/default.css', attrs: ['id': 'cerulean']

    }



    simplenso {
        dependsOn 'jquery,datatables'
        resource url: 'scripts/simplenso/simplenso.js'
        resource url: 'css/simplenso.css'

    }

    fullcalendar {
        dependsOn 'jquery'
        resource url: 'scripts/fullcalendar/fullcalendar/fullcalendar.css'
        resource url: 'scripts/fullcalendar/fullcalendar/fullcalendar.min.js'
    }

    'bootstrap-datepicker' {
        dependsOn 'bootstrapjs'
        resource url: 'scripts/datepicker/css/datepicker.css'
        resource url: 'scripts/datepicker/js/bootstrap-datepicker.js'

    }

    'bootstrap-image-gallery' {
        dependsOn 'bootstrapjs,jquery'
        resource url:'http://blueimp.github.com/JavaScript-Templates/tmpl.min.js'

        resource url: 'http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js'
        resource url: 'http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js'

        resource url: 'http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css'
        resource url: 'http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js'

    }

    'jquery-file-upload' {
        dependsOn 'jquery,jquery-ui-sortable'
        resource url: 'scripts/blueimp-jQuery-File-Upload/css/jquery.fileupload-ui.css'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/jquery.iframe-transport.js'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload.js'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload-ip.js'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/jquery.fileupload-ui.js'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/main.js'
        resource url: 'scripts/blueimp-jQuery-File-Upload/js/cors/jquery.xdr-transport.js', wrapper: { s -> "<!--[if gte IE 8]>$s<![endif]-->" }
    }

    datatables {
        dependsOn 'jquery'
        resource url: 'scripts/DataTables/media/js/jquery.dataTables.js'
    }

    googleviz {
        dependsOn 'jquery'
        resource url: 'http://www.google.com/jsapi', disposition: 'head', attrs: [type: 'js']
        resource url: 'scripts/google-viz/html5.js', linkOverride: 'http://html5shim.googlecode.com/svn/trunk/html5.js', wrapper: { s -> "<!--[if lt IE 9]>$s<![endif]-->" }
    }

    'jquery-ui-sortable' {
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.core.min.js'
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.widget.min.js'
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.mouse.min.js'
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.sortable.min.js'

    }

    'jquery-ui-draggable' {
        dependsOn 'jquery-ui-sortable'
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.draggable.min.js'
        resource url: 'scripts/jquery-ui/ui/minified/jquery.ui.droppable.min.js'
    }

    bootbox {
        dependsOn 'bootstrapjs'
        resource url: 'scripts/bootbox/bootbox.min.js'
    }

    ckeditor {
        dependsOn 'jquery'
        resource url: 'scripts/ckeditor/ckeditor.js'
        resource url: 'scripts/ckeditor/adapters/jquery.js'
    }

    jqcookie {
        dependsOn 'jquery'
        resource url: 'scripts/jquery.cookie/jquery.cookie.js'
    }

    chosenmultiselect {
        dependsOn 'jquery'
        resource url: 'scripts/chosen/chosen/chosen.jquery.min.js'
        resource url: 'scripts/chosen/chosen/chosen.intenso.css'
    }

    uniform {
        dependsOn 'jquery'
        resource url: 'scripts/uniform/jquery.uniform.min.js'
        resource url: 'scripts/uniform/css/uniform.default.css'
    }

    all {
        dependsOn 'bootstrap-image-gallery,uniform,chosenmultiselect,jqcookie,ckeditor,bootbox,jquery-ui-draggable,jquery-ui-sortable,fullcalendar,bootstrap-datepicker,jquery-file-upload,datatables,simplenso,googleviz'
    }


    underscoreJS{
        dependsOn 'jquery'
        resource url: 'js/underscore-min.js'
    }

}