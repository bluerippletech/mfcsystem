import com.ripple.security.AppRole
import com.ripple.security.AppUser
import com.ripple.security.AppUserAppRole

class BootStrap {

    def init = { servletContext ->
//        Requestmap.findOrSaveWhere(url: '', configAttribute: '')
//        Requestmap.findOrSaveWhere(url: '/*', configAttribute: 'IS_AUTHENTICATED_FULLY')


        if (!AppRole.findByAuthority("ROLE_ADMIN")) {
            def adminAppRole = new AppRole(authority: 'ROLE_ADMIN',roleName: 'Administrator')
            adminAppRole.save(flush: true)
            def userAppRole = new AppRole(authority: 'ROLE_USER',roleName: 'Standard')
            userAppRole.save(flush: true)
//        def pass = DigestUtils.shaHex("admin")
            def user = new AppUser(username: 'admin', password: "admin", enabled: true,
                    accountExpired: false, accountLocked: false, passwordExpired: false)
            user.save(flush: true)
            def appuserrole = new AppUserAppRole(appRole: adminAppRole, appUser: user)
            appuserrole.save(flush: true)
        }
    }
    def destroy = {
    }
}
