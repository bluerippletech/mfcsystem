package com.ripple.security



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class AppUserController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [appUserInstanceList: AppUser.list(params), appUserInstanceTotal: AppUser.count()]
}

def create() {
    [appUserInstance: new AppUser(params)]
}

def save() {
    def appUserInstance = new AppUser(params)
    if (!appUserInstance.save(flush: true)) {
        render(view: "create", model: [appUserInstance: appUserInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
    redirect(action: "show", id: appUserInstance.id)
}

def show() {
    def appUserInstance = AppUser.get(params.id)
    if (!appUserInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "list")
        return
    }

    [appUserInstance: appUserInstance]
}

def edit() {
    def appUserInstance = AppUser.get(params.id)
    if (!appUserInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "list")
        return
    }

    [appUserInstance: appUserInstance]
}

def update() {
    def appUserInstance = AppUser.get(params.id)
    if (!appUserInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (appUserInstance.version > version) {
                appUserInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'appUser.label', default: 'AppUser')] as Object[],
                        "Another user has updated this AppUser while you were editing")
            render(view: "edit", model: [appUserInstance: appUserInstance])
            return
        }
    }

    appUserInstance.properties = params

    if (!appUserInstance.save(flush: true)) {
        render(view: "edit", model: [appUserInstance: appUserInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'appUser.label', default: 'AppUser'), appUserInstance.id])
    redirect(action: "show", id: appUserInstance.id)
}

def delete() {
    def appUserInstance = AppUser.get(params.id)
    if (!appUserInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "list")
        return
    }

    try {
        appUserInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appUser.label', default: 'AppUser'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = AppUser.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('username',"%${sSearch}%")
                        
                        ilike('password',"%${sSearch}%")
                        
                        ilike('accountExpired',"%${sSearch}%")
                        
                        ilike('accountLocked',"%${sSearch}%")
                        
                        ilike('enabled',"%${sSearch}%")
                        
                        ilike('passwordExpired',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = AppUser.count();
    def totalDisplayRecords = AppUser.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('username',"%${sSearch}%")
                        
                        ilike('password',"%${sSearch}%")
                        
                        ilike('accountExpired',"%${sSearch}%")
                        
                        ilike('accountLocked',"%${sSearch}%")
                        
                        ilike('enabled',"%${sSearch}%")
                        
                        ilike('passwordExpired',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.username,
                        
                        1:it.password,
                        
                        2:it.accountExpired,
                        
                        3:it.accountLocked,
                        
                        4:it.enabled,
                        
                        5:it.passwordExpired,
                        
                        6:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
