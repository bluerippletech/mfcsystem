package com.ripple.security



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class AppRoleController {

static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

def index() {
    redirect(action: "list", params: params)
}

def list() {
    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [appRoleInstanceList: AppRole.list(params), appRoleInstanceTotal: AppRole.count()]
}

def create() {
    [appRoleInstance: new AppRole(params)]
}

def save() {
    def appRoleInstance = new AppRole(params)
    if (!appRoleInstance.save(flush: true)) {
        render(view: "create", model: [appRoleInstance: appRoleInstance])
        return
    }

    flash.message = message(code: 'default.created.message', args: [message(code: 'appRole.label', default: 'AppRole'), appRoleInstance.id])
    redirect(action: "show", id: appRoleInstance.id)
}

def show() {
    def appRoleInstance = AppRole.get(params.id)
    if (!appRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "list")
        return
    }

    [appRoleInstance: appRoleInstance]
}

def edit() {
    def appRoleInstance = AppRole.get(params.id)
    if (!appRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "list")
        return
    }

    [appRoleInstance: appRoleInstance]
}

def update() {
    def appRoleInstance = AppRole.get(params.id)
    if (!appRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "list")
        return
    }

    if (params.version) {
        def version = params.version.toLong()
        if (appRoleInstance.version > version) {
                appRoleInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'appRole.label', default: 'AppRole')] as Object[],
                        "Another user has updated this AppRole while you were editing")
            render(view: "edit", model: [appRoleInstance: appRoleInstance])
            return
        }
    }

    appRoleInstance.properties = params

    if (!appRoleInstance.save(flush: true)) {
        render(view: "edit", model: [appRoleInstance: appRoleInstance])
        return
    }

    flash.message = message(code: 'default.updated.message', args: [message(code: 'appRole.label', default: 'AppRole'), appRoleInstance.id])
    redirect(action: "show", id: appRoleInstance.id)
}

def delete() {
    def appRoleInstance = AppRole.get(params.id)
    if (!appRoleInstance) {
        flash.message = message(code: 'default.not.found.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "list")
        return
    }

    try {
        appRoleInstance.delete(flush: true)
        flash.message = message(code: 'default.deleted.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "list")
    }
    catch (DataIntegrityViolationException e) {
        flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appRole.label', default: 'AppRole'), params.id])
        redirect(action: "show", id: params.id)
    }
}

def listJSON(){
    def columns = params.sColumns.tokenize(",");
    def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
    def sortOrder = params.sSortDir_0;
    def sEcho = Integer.valueOf(params.sEcho)+1
    def sSearch = params.sSearch

    def listing = AppRole.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int){
        order(sortColumn,sortOrder)
        if (sSearch){
            or{
                
                        ilike('authority',"%${sSearch}%")
                        
            }
        }
    }
    def totalRecords = AppRole.count();
    def totalDisplayRecords = AppRole.createCriteria().count{
        if (sSearch){
            or{
                
                        ilike('authority',"%${sSearch}%")
                        
            }
        }

    }

    def jsonList = [];
    jsonList = listing.collect {
        [
                
                        0:it.authority,
                        
                        1:'',
                        "DT_RowId": it.id
                        
        ]
    }
    def data = [
            sEcho:sEcho,
            iTotalRecords:totalRecords,
            iTotalDisplayRecords:totalDisplayRecords,
            aaData:jsonList];
    render data as JSON;
}
}
