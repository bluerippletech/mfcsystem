package com.ripple.master



import org.springframework.dao.DataIntegrityViolationException
import grails.converters.JSON

class CustomerController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [customerInstanceList: Customer.list(params), customerInstanceTotal: Customer.count()]
    }

    def create() {
        [customerInstance: new Customer(params)]
    }

    def save() {
        def customerInstance = new Customer(params)
        if (!customerInstance.save(flush: true)) {
            render(view: "create", model: [customerInstance: customerInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'customer.label', default: 'Customer'), customerInstance.id])
        redirect(action: "show", id: customerInstance.id)
    }

    def show() {
        def customerInstance = Customer.get(params.id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "list")
            return
        }

        [customerInstance: customerInstance]
    }

    def edit() {
        def customerInstance = Customer.get(params.id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "list")
            return
        }

        [customerInstance: customerInstance]
    }

    def update() {
        def customerInstance = Customer.get(params.id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (customerInstance.version > version) {
                customerInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'customer.label', default: 'Customer')] as Object[],
                        "Another user has updated this Customer while you were editing")
                render(view: "edit", model: [customerInstance: customerInstance])
                return
            }
        }

        customerInstance.properties = params

        if (!customerInstance.save(flush: true)) {
            render(view: "edit", model: [customerInstance: customerInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'customer.label', default: 'Customer'), customerInstance.id])
        redirect(action: "show", id: customerInstance.id)
    }

    def delete() {
        def customerInstance = Customer.get(params.id)
        if (!customerInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "list")
            return
        }

        try {
            customerInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'customer.label', default: 'Customer'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def listJSON() {
        def columns = params.sColumns.tokenize(",");
        def sortColumn = columns.get(Integer.parseInt(params.iSortCol_0));
        def sortOrder = params.sSortDir_0;
        def sEcho = Integer.valueOf(params.sEcho) + 1
        def sSearch = params.sSearch

        def listing = Customer.createCriteria().list(max: params.iDisplayLength as int, offset: params.iDisplayStart as int) {
            order(sortColumn, sortOrder)
            if (sSearch) {
                or {
                    ilike('companyName', "%${sSearch}%")
                    ilike('companyAddress', "%${sSearch}%")
                    ilike('deliveryAddress', "%${sSearch}%")
                    ilike('telephoneNumber', "%${sSearch}%")
                    ilike('faxNumber', "%${sSearch}%")
                }
            }
        }
        def totalRecords = Customer.count();
        def totalDisplayRecords = Customer.createCriteria().count {
            if (sSearch) {
                or {
                    ilike('companyName', "%${sSearch}%")
                    ilike('companyAddress', "%${sSearch}%")
                    ilike('deliveryAddress', "%${sSearch}%")
                    ilike('telephoneNumber', "%${sSearch}%")
                    ilike('faxNumber', "%${sSearch}%")
                }
            }

        }

        def jsonList = [];
        jsonList = listing.collect {
            [

                    0: it.companyName,

                    1: it.companyAddress,

                    2: it.deliveryAddress,

                    3: it.telephoneNumber,

                    4: it.faxNumber,

                    5: '',
                    "DT_RowId": it.id

            ]
        }
        def data = [
                sEcho: sEcho,
                iTotalRecords: totalRecords,
                iTotalDisplayRecords: totalDisplayRecords,
                aaData: jsonList];
        render data as JSON;
    }

    def remoteAddContactPerson() {
        def customer = Customer.get(params.id)
        def contactPerson = params.contactPerson

        if (!customer) {
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Customer cannot be found."
            return
        }

        if (!contactPerson) {
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Contact person cannot be empty."
            return

        } else {
            customer.addToContactPersons(contactPerson)
            def contactPersons = customer.getContactPersons() as List

            response.status = response.SC_OK;
            render contactPersons.sort() as JSON;
        }
    }

    def remoteDeleteContactPerson() {
        def customer = Customer.get(params.id)
        def contactPerson = params.contactPerson

        if (!customer) {
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Customer cannot be found."
            return
        }

        if (!contactPerson) {
            response.status = response.SC_INTERNAL_SERVER_ERROR
            render "Contact person cannot be empty."
            return

        } else {
            customer.removeFromContactPersons(contactPerson)
            def contactPersons = customer.getContactPersons() as List
            response.status = response.SC_OK;
            render contactPersons.sort() as JSON;
        }


    }
}
