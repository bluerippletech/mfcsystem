package com.ripple.security

import com.ripple.master.Base

//@gorm.AuditStamp
class AppRole extends Base{

	String authority
    String roleName;

	static mapping = {
        id generator:'uuid'
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
        roleName(blank: false)
	}

    public String toString(){
        return roleName;
    }
}
