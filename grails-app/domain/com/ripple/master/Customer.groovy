package com.ripple.master


@gorm.AuditStamp
class Customer extends Base {

    String companyName
    String companyAddress
    String deliveryAddress

    String telephoneNumber
    String faxNumber

    static hasMany = [contactPersons:String]

    static constraints = {
        companyName(blank:false,nullable:false,maxSize:255)
        companyAddress(blank: false,nullable:false,maxSize:2000)
        deliveryAddress(blank:true,nullable:true,maxSize:2000)
        telephoneNumber(blank:false,nullable:false,maxSize: 50)
        faxNumber(blank:false,nullable:false,maxSize: 50)
    }
}
