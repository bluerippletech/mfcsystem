<% import grails.persistence.Event %>
<%=packageName%>
<!doctype html>
<html>
<head>
    <meta charset="utf-8" name="layout" content="main"/>
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <r:require modules="all"/>
</head>

<body>
<div class="row-fluid">
    <div class="span1 action-btn round-all">
        <a href="\${createLink(uri: '/')}">
            <div><i class="icon-home"></i></div>

            <div><strong><g:message code="default.home.label"/></strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="\${createLink(action: 'list')}">
            <div><i class="icon-align-justify"></i></div>

            <div><strong>List</strong></div>
        </a>
    </div>

    <div class="span1 action-btn round-all">
        <a href="\${createLink(action: 'create')}">
            <div><i class="icon-pencil"></i></div>

            <div><strong>Create</strong></div>
        </a>
    </div>
</div>
<g:if test="\${flash.message}">
    <div class="alert alert-success" role="status">
        <a class="close" data-dismiss="alert" href="#">&times;</a>
        \${flash.message}
    </div>
</g:if>
<div class="row-fluid">
    <div class="span12 column" id="edit-${domainClass.propertyName}" role="main">
        <!-- Portlet: Form Control States -->
        <div class="box" id="box-0">
            <h4 class="box-header round-top"><g:message code="default.show.label" args="[entityName]"/>
                <a class="box-btn" title="close"><i class="icon-remove"></i></a>
                <a class="box-btn" title="toggle"><i class="icon-minus"></i></a>
                <a class="box-btn" title="config" data-toggle="modal" href="#box-config-modal"><i class="icon-cog"></i>
                </a>
            </h4>

            <div class="box-container-toggle">
                <div class="box-content">
                    <fieldset>
                        <legend><g:message code="default.show.label" args="[entityName]"/></legend>
                        <div class="row-fluid">
                            <div class="span6 column">
                                <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
                                allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
                                props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
                                Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                                props.eachWithIndex { p,i ->
                                 if (i%2>0){
                                %>
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="${p.name}-label" class="control-label"><g:message
                                                    code="${domainClass.propertyName}.${p.name}.label"
                                                    default="${p.naturalName}"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                <% if (p.isEnum()) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue
                                                        bean="\${${propertyName}}" field="${p.name}"/></span>
                                                <% } else if (p.oneToMany || p.manyToMany) { %>
                                                <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
                                                    <span class="property-value" aria-labelledby="${p.name}-label"><g:link
                                                            controller="${p.referencedDomainClass?.propertyName}" action="show"
                                                            id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                <% } else if (p.manyToOne || p.oneToOne) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:link
                                                        controller="${p.referencedDomainClass?.propertyName}" action="show"
                                                        id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></span>
                                                <% } else if (p.type == Boolean || p.type == boolean) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:formatBoolean
                                                        boolean="\${${propertyName}?.${p.name}}"/></span>
                                                <% } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:formatDate
                                                        date="\${${propertyName}?.${p.name}}" format="MMMMM dd, yyyy"/></span>
                                                <% } else if (!p.type.isArray()) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue
                                                        bean="\${${propertyName}}" field="${p.name}"/></span>
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                <%      }
                                        } %>
                            </div>
                            <div class="span6 column">
                                <% excludedProps = Event.allEvents.toList() << 'id' << 'version'
                                allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
                                props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) }
                                Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                                props.eachWithIndex { p,i ->
                                if (i%2==0){
                                %>
                                    <div class="form-horizontal">
                                        <div class="control-group">
                                            <h5 id="${p.name}-label" class="control-label"><g:message
                                                    code="${domainClass.propertyName}.${p.name}.label"
                                                    default="${p.naturalName}"/></h5>
                                            <div class="controls" style="margin-top:4px">
                                                <% if (p.isEnum()) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue
                                                        bean="\${${propertyName}}" field="${p.name}"/></span>
                                                <% } else if (p.oneToMany || p.manyToMany) { %>
                                                <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
                                                    <span class="property-value" aria-labelledby="${p.name}-label"><g:link
                                                            controller="${p.referencedDomainClass?.propertyName}" action="show"
                                                            id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></span>
                                                </g:each>
                                                <% } else if (p.manyToOne || p.oneToOne) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:link
                                                        controller="${p.referencedDomainClass?.propertyName}" action="show"
                                                        id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></span>
                                                <% } else if (p.type == Boolean || p.type == boolean) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:formatBoolean
                                                        boolean="\${${propertyName}?.${p.name}}"/></span>
                                                <% } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:formatDate
                                                        date="\${${propertyName}?.${p.name}}" format="MMMMM dd, yyyy"/></span>
                                                <% } else if (!p.type.isArray()) { %>
                                                <span class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue
                                                        bean="\${${propertyName}}" field="${p.name}"/></span>
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                <% }} %>

                            </div>
                        </div>

                        <div class="form-actions form-horizontal">
                            <g:form>
                                <g:hiddenField name="id" value="\${${propertyName}?.id}"/>
                                %{--<g:link class="edit" action="edit" id="\${${propertyName}?.id}"><g:message--}%
                                        %{--code="default.button.edit.label" default="Edit"/></g:link>--}%
                                <g:actionSubmit class="btn btn-primary" action="edit"
                                                value="\${message(code: 'default.button.edit.label', default: 'Edit')}"/>

                                <g:actionSubmit class="delete btn-danger" action="delete"
                                                value="\${message(code: 'default.button.delete.label', default: 'Delete')}"
                                                onclick="return confirm('\${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                            </g:form>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div><!--/span-->

    </div>
</div>
</body>
</html>
