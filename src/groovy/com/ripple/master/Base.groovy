package com.ripple.master


abstract class Base {

    String id

    static auditable = [ignore:['version','createdDate','editedDate','editedBy','createdBy']]

    static mapping = {
        id generator:'uuid'
        cache true
    }
}

