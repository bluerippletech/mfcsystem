package com.ripple.util

import org.joda.time.LocalDate
import org.springframework.beans.PropertyEditorRegistrar
import org.springframework.beans.PropertyEditorRegistry
import org.springframework.beans.propertyeditors.CustomDateEditor

import java.text.SimpleDateFormat

/**
 * Created by IntelliJ IDEA.
 * User: onats
 * Date: 11 3, 09
 * Time: 5:18:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomPropertyEditorRegistrar implements
PropertyEditorRegistrar {
    public void registerCustomEditors(PropertyEditorRegistry registry) {
        registry.registerCustomEditor(LocalDate.class, new LocalDateEditor());
        String dateFormat = 'M/d/yyyy';
        registry.registerCustomEditor(Date, new CustomDateEditor(new SimpleDateFormat(dateFormat), true));

    }
}