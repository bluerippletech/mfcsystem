package com.ripple.security



import org.junit.*
import grails.test.mixin.*

@TestFor(AppUserAppRoleController)
@Mock(AppUserAppRole)
class AppUserAppRoleControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/appUserAppRole/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.appUserAppRoleInstanceList.size() == 0
        assert model.appUserAppRoleInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.appUserAppRoleInstance != null
    }

    void testSave() {
        controller.save()

        assert model.appUserAppRoleInstance != null
        assert view == '/appUserAppRole/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/appUserAppRole/show/1'
        assert controller.flash.message != null
        assert AppUserAppRole.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/appUserAppRole/list'


        populateValidParams(params)
        def appUserAppRole = new AppUserAppRole(params)

        assert appUserAppRole.save() != null

        params.id = appUserAppRole.id

        def model = controller.show()

        assert model.appUserAppRoleInstance == appUserAppRole
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/appUserAppRole/list'


        populateValidParams(params)
        def appUserAppRole = new AppUserAppRole(params)

        assert appUserAppRole.save() != null

        params.id = appUserAppRole.id

        def model = controller.edit()

        assert model.appUserAppRoleInstance == appUserAppRole
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/appUserAppRole/list'

        response.reset()


        populateValidParams(params)
        def appUserAppRole = new AppUserAppRole(params)

        assert appUserAppRole.save() != null

        // test invalid parameters in update
        params.id = appUserAppRole.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/appUserAppRole/edit"
        assert model.appUserAppRoleInstance != null

        appUserAppRole.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/appUserAppRole/show/$appUserAppRole.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        appUserAppRole.clearErrors()

        populateValidParams(params)
        params.id = appUserAppRole.id
        params.version = -1
        controller.update()

        assert view == "/appUserAppRole/edit"
        assert model.appUserAppRoleInstance != null
        assert model.appUserAppRoleInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/appUserAppRole/list'

        response.reset()

        populateValidParams(params)
        def appUserAppRole = new AppUserAppRole(params)

        assert appUserAppRole.save() != null
        assert AppUserAppRole.count() == 1

        params.id = appUserAppRole.id

        controller.delete()

        assert AppUserAppRole.count() == 0
        assert AppUserAppRole.get(appUserAppRole.id) == null
        assert response.redirectedUrl == '/appUserAppRole/list'
    }
}
